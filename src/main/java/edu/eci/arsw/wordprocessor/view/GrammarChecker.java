/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.wordprocessor.view;

/**
 *
 * @author oscar
 */
public class GrammarChecker {
    SpellChecker sc;

    public SpellChecker getSc() {
        return sc;
    }

    public void setSc(SpellChecker sc) {
        this.sc = sc;
    }
    
    public String check(String palabra){
        return sc.checkSpell();
    }
    
    
}

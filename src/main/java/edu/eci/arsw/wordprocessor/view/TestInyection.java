/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.wordprocessor.view;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author oscar
 */
public class TestInyection {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("applicationContext.xml");
        GrammarChecker gc=ac.getBean(GrammarChecker.class);
        System.out.println(gc.check("la la la"));
        
    }
    
}
